The Anonymous login node assign module allows users to assign noded they have created anonymously to an account they have access to after they login or register.

Installation
============

* Download & enable the module in the normal manner.

Usage
=====

* Configure the applicable content types and cron unpublish values at /admin/config/anon-login-node-assign.
