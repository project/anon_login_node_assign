<?php

/**
 * @file
 * Administrative page callbacks for the Anonymous login node assign module.
 */

/**
 * Menu callback for settings form.
 */
function anon_login_node_assign_admin_settings_form($form, $form_state) {

  // Applicable content types.
  $options = array();
  foreach (node_type_get_types() as $type) {
    $options[$type->type] = $type->name;
  }
  $form['anon_login_node_assign_content_types'] = array(
    '#type'          => 'select',
    '#title'         => t('Applicable content types'),
    '#options'       => $options,
    '#multiple'      => TRUE,
    '#size'          => 10,
    '#description'   => t('Content types which apply to anonymous login node assign.'),
    '#default_value' => variable_get('anon_login_node_assign_content_types', array()),
  );

  // Cron unpublish interval.
  $options = array(0 => 'Never');
  $hour = 60 * 60;
  for ($i = $hour * 4; $i <= $hour * 168; $i += $hour * 4) {
    $options[$i] = format_interval($i, 2);
  }
  $form['anon_login_node_assign_unpublish_interval'] = array(
    '#type'          => 'select',
    '#title'         => t('Cron unpublish interval'),
    '#options'       => $options,
    '#description'   => t('Interval between unassigned content being changed and unpublished by cron.'),
    '#default_value' => variable_get('anon_login_node_assign_unpublish_interval', 0),
  );

  // Cron delete limit.
  $options = array();
  for ($i = 1; $i <= 50; $i++) {
    $options[$i] = $i;
  }
  $form['anon_login_node_assign_unpublish_limit'] = array(
    '#type'          => 'select',
    '#title'         => t('Cron unpublish volume'),
    '#options'       => $options,
    '#description'   => t('Volume of unassigned content being unpublished per cron.'),
    '#default_value' => variable_get('anon_login_node_assign_unpublish_limit', 5),
  );

  return system_settings_form($form);
}
